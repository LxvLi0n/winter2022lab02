public class MethodsTest{
	public static void main(String [] args){
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(2,8.0);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		double l = sumSquareRoot(45,6);
		System.out.println(l);
		String s1 = ("hello");
		String s2 = ("goodbye");
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));

	}
	public static void methodNoInputNoReturn(){
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
		
	}
	public static void methodOneInputNoReturn(int x){
		System.out.println("Inside the method one input no return" +x);
	}
	public static void methodTwoInputNoReturn(int a , double d){
		System.out.println("Inside the method two input no return");
	}
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int a,int b){
		return Math.sqrt(a+b);
	}
	
}

